package com.epam.view;

import com.epam.controller.Controller;
import com.epam.view.menuServise.Menu;

import java.util.LinkedHashMap;
import java.util.Map;

public class MainMenu implements Menu {
    private Controller controller;

    public MainMenu() {
        controller = new Controller();
    }

    @Override
    public String initNameOfMenu() {
        return "MENU";
    }

    @Override
    public Map<String, String> initDisplay() {
        return new LinkedHashMap<String, String>() {
            {
                put("1", "1\tParse from file DOM");
                put("2", "2\tParse from file SAX");
                put("3", "3\tParse from file STAX");
                put("Q", "Q\texit");
            }
        };
    }

    @Override
    public Map<String, Runnable> initExecution() {
        return new LinkedHashMap<String, Runnable>() {
            {
                put("1", () -> controller.getTouristVouchersDOM("touristVouchers"));
                put("2", () -> controller.getTouristVouchersSAX("touristVouchers"));
                put("3", () -> controller.getTouristVouchersSTAX("touristVouchers"));
                put("Q", () -> System.out.println("\nBye!!"));
            }
        };
    }
}
