package com.epam.model.action.parsers.stax;

import com.epam.model.action.MyParser;
import com.epam.model.entity.Apartment;
import com.epam.model.entity.Hotel;
import com.epam.model.entity.TouristVoucher;
import com.epam.model.entity.hotelService.HotelService;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.List;

public class STAXReader {
    private XMLEventReader xmlEventReader;
    private List<TouristVoucher> touristVouchers;

    public STAXReader(XMLEventReader xmlEventReader) {
        this.xmlEventReader = xmlEventReader;
    }

    public List<TouristVoucher> getTouristVouchers() {
        return touristVouchers;
    }

    public void readData() throws XMLStreamException {
        TouristVoucher touristVoucher = new TouristVoucher();
        touristVoucher.setHotel(new Hotel());
        touristVoucher.getHotel().setApartment(new Apartment());
        touristVoucher.getHotel().setHotelService(new HotelService());
        while (xmlEventReader.hasNext()) {
            XMLEvent xmlEvent = xmlEventReader.nextEvent();
            if (xmlEvent.isStartElement()) {
                StartElement startElement = xmlEvent.asStartElement();
                String name = startElement.getName().getLocalPart();
                switch (name) {
                    case "touristVoucher":
                        Attribute idAttr = startElement.getAttributeByName(new QName("id"));
                        if (idAttr != null) {
                            touristVoucher.setId(Integer.parseInt(idAttr.getValue()));
                        }
                        break;
                    case "type":
                        xmlEvent = xmlEventReader.nextEvent();
                        touristVoucher.setTripType(xmlEvent.asCharacters().getData());
                        break;
                    case "country":
                        xmlEvent = xmlEventReader.nextEvent();
                        touristVoucher.setCountry(xmlEvent.asCharacters().getData());
                        break;
                    case "quantityDays":
                        xmlEvent = xmlEventReader.nextEvent();
                        touristVoucher.setQuantityDays(Integer.parseInt(xmlEvent.asCharacters().getData()));
                        break;
                    case "quantityNights":
                        xmlEvent = xmlEventReader.nextEvent();
                        touristVoucher.setQuantityNights(Integer.parseInt(xmlEvent.asCharacters().getData()));
                        break;
                    case "transportType":
                        xmlEvent = xmlEventReader.nextEvent();
                        touristVoucher.setTransportType(MyParser.getTransportType(xmlEvent.asCharacters().getData()));
                        break;
                    case "hotelName":
                        xmlEvent = xmlEventReader.nextEvent();
                        touristVoucher.getHotel().setHotelName(xmlEvent.asCharacters().getData());
                        break;
                    case "hotelRate":
                        xmlEvent = xmlEventReader.nextEvent();
                        touristVoucher.getHotel().setHotelRating(Double.parseDouble(xmlEvent.asCharacters().getData()));
                        break;
                    case "idApartment":
                        xmlEvent = xmlEventReader.nextEvent();
                        touristVoucher.getHotel().getApartment()
                                .setIdNumber(Integer.parseInt(xmlEvent.asCharacters().getData()));
                        break;
                    case "quantityPersons":
                        xmlEvent = xmlEventReader.nextEvent();
                        touristVoucher.getHotel().getApartment()
                                .setQuantityPerson(Integer.parseInt(xmlEvent.asCharacters().getData()));
                        break;
                    case "meal":
                        xmlEvent = xmlEventReader.nextEvent();
                        touristVoucher.getHotel().getHotelService()
                                .setMeal(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                        break;
                    case "tv":
                        xmlEvent = xmlEventReader.nextEvent();
                        touristVoucher.getHotel().getHotelService()
                                .setTv(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                        break;
                    case "conditioner":
                        xmlEvent = xmlEventReader.nextEvent();
                        touristVoucher.getHotel().getHotelService()
                                .setConditioner(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                        break;
                }
            } else if(xmlEvent.isEndElement()){
                EndElement endElement = xmlEvent.asEndElement();
                if("touristVoucher".equals(endElement.getName().getLocalPart())){
                    touristVouchers.add(touristVoucher);
                }
            }
        }
    }
}
