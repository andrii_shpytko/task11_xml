package com.epam.model.action.parsers.sax;

import com.epam.model.action.MyParser;
import com.epam.model.entity.Apartment;
import com.epam.model.entity.Hotel;
import com.epam.model.entity.TouristVoucher;
import com.epam.model.entity.hotelService.HotelService;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SAXHandler extends DefaultHandler {
    private List<TouristVoucher> touristVouchers;
    private TouristVoucher touristVoucher;
    private boolean type;
    private boolean country;
    private boolean quantityDays;
    private boolean quantityNights;
    private boolean transportType;
    private boolean hotelName;
    private boolean hotelRating;
    private boolean idApartment;
    private boolean quantityPersons;
    private boolean meal;
    private boolean tv;
    private boolean conditioner;

    SAXHandler() {
        touristVouchers = new ArrayList<>();
    }

    public List<TouristVoucher> getTouristVouchers() {
        return touristVouchers;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if ("touristVoucher".equalsIgnoreCase(qName)) {
            touristVoucher = new TouristVoucher();
            touristVoucher.setHotel(new Hotel());
            touristVoucher.getHotel().setApartment(new Apartment());
            touristVoucher.getHotel().setHotelService(new HotelService());
            touristVoucher.setId(Integer.parseInt(attributes.getValue("id")));
        } else if ("type".equalsIgnoreCase(qName)) {
            type = true;
        } else if ("country".equalsIgnoreCase(qName)) {
            country = true;
        } else if ("quantityDays".equalsIgnoreCase(qName)) {
            quantityDays = true;
        } else if ("quantityNights".equalsIgnoreCase(qName)) {
            quantityNights = true;
        } else if ("transportType".equalsIgnoreCase(qName)) {
            transportType = true;
        } else if ("hotelName".equalsIgnoreCase(qName)) {
            hotelName = true;
        } else if ("hotelRating".equalsIgnoreCase(qName)) {
            hotelRating = true;
        } else if ("idApartment".equalsIgnoreCase(qName)) {
            idApartment = true;
        } else if ("quantityPersons".equalsIgnoreCase(qName)) {
            quantityPersons = true;
        } else if ("meal".equalsIgnoreCase(qName)) {
            meal = true;
        } else if ("tv".equalsIgnoreCase(qName)) {
            tv = true;
        } else if ("conditioner".equalsIgnoreCase(qName)) {
            conditioner = true;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if ("touristVoucher".equalsIgnoreCase(qName)) {
            touristVouchers.add(touristVoucher);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (type) {
            touristVoucher.setTripType(new String(ch, start, length));
            type = false;
        } else if (country) {
            touristVoucher.setCountry(new String(ch, start, length));
            country = false;
        } else if (quantityDays) {
            touristVoucher.setQuantityDays(Integer.parseInt(new String(ch, start, length)));
            quantityDays = false;
        } else if (quantityNights) {
            touristVoucher.setQuantityNights(Integer.parseInt(new String(ch, start, length)));
            quantityNights = false;
        } else if (transportType) {
            touristVoucher.setTransportType(MyParser.getTransportType(new String(ch, start, length)));
            transportType = false;
        } else if (hotelName) {
            touristVoucher.getHotel().setHotelName(new String(ch, start, length));
            hotelName = false;
        } else if (hotelRating) {
            touristVoucher.getHotel().setHotelRating(Double.parseDouble(new String(ch, start, length)));
            hotelRating = false;
        } else if (idApartment) {
            touristVoucher.getHotel().getApartment()
                    .setIdNumber(Integer.parseInt(new String(ch, start, length)));
            idApartment = false;
        } else if (quantityPersons) {
            touristVoucher.getHotel().getApartment()
                    .setQuantityPerson(Integer.parseInt(new String(ch, start, length)));
            quantityPersons = false;
        } else if (meal) {
            touristVoucher.getHotel().getHotelService()
                    .setMeal(Boolean.parseBoolean(new String(ch, start, length)));
            meal = false;
        } else if (tv) {
            touristVoucher.getHotel().getHotelService()
                    .setTv(Boolean.parseBoolean(new String(ch, start, length)));
            tv = false;
        } else if (conditioner) {
            touristVoucher.getHotel().getHotelService()
                    .setConditioner(Boolean.parseBoolean(new String(ch, start, length)));
            conditioner = false;
        }
    }
}
