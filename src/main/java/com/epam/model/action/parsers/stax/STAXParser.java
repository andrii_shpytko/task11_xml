package com.epam.model.action.parsers.stax;

import com.epam.model.action.MyParser;
import com.epam.model.entity.TouristVoucher;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

public class STAXParser extends MyParser {
    private STAXReader staxReader;

    public STAXParser(String xmlFileName, String xsdFileName) throws FileNotFoundException, XMLStreamException {
        super(xmlFileName, xsdFileName);
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        FileInputStream fileInputStream = new FileInputStream(getXmlFile());
        staxReader = new STAXReader(inputFactory.createXMLEventReader(fileInputStream));
    }

    @Override
    public List<TouristVoucher> parseFromFile() throws XMLStreamException {
        staxReader.readData();
        return staxReader.getTouristVouchers();
    }
}
