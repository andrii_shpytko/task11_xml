package com.epam.model.action.parsers.dom;

import com.epam.model.action.MyParser;
import com.epam.model.entity.TouristVoucher;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.List;

public class DOMParser extends MyParser {
    private DOMReader domReader;

    public DOMParser(String xmlFileName, String xsdFileName)
            throws IOException, SAXException, ParserConfigurationException {
        super(xmlFileName, xsdFileName);
        DOMCreator domCreator = new DOMCreator(getXmlFile());
        domReader = new DOMReader(domCreator.getDocument());
    }

    @Override
    public List<TouristVoucher> parseFromFile() {
        domReader.readData();
        return domReader.getTouristVoucher();
    }
}
