package com.epam.model.action.parsers.sax;

import com.epam.model.action.MyParser;
import com.epam.model.entity.TouristVoucher;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class MySAXParser extends MyParser {
    private SAXParserFactory saxParserFactory;
    private SAXHandler saxHandler;
    private SAXParser saxParser;

    public MySAXParser(String xmlFileName, String xsdFileName) throws FileNotFoundException,
            SAXException, ParserConfigurationException {
        super(xmlFileName, xsdFileName);
        saxParserFactory = SAXParserFactory.newInstance();
        saxParserFactory.setSchema(SAXValidator.createSchema(getXsdFile()));
        saxParser = saxParserFactory.newSAXParser();
        saxHandler = new SAXHandler();
    }

    @Override
    public List<TouristVoucher> parseFromFile() throws IOException, SAXException {
        saxParser.parse(getXmlFile(), saxHandler);
        return saxHandler.getTouristVouchers();
    }
}
