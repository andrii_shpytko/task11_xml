package com.epam.model.action.parsers.sax;

import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;

public class SAXValidator {
    public static Schema createSchema(File xsdFile) throws SAXException {
        String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
        SchemaFactory schemaFactory = SchemaFactory.newInstance(language);
        return schemaFactory.newSchema(xsdFile);
    }
}
