package com.epam.model.action.parsers.dom;

import com.epam.model.action.MyParser;
import com.epam.model.entity.Apartment;
import com.epam.model.entity.Hotel;
import com.epam.model.entity.TouristVoucher;
import com.epam.model.entity.TransportType;
import com.epam.model.entity.hotelService.HotelService;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class DOMReader {
    private Document document;
    private List<TouristVoucher> touristVoucher;

    public DOMReader(Document document) {
        this.document = document;
        touristVoucher = new ArrayList<>();
    }

    public List<TouristVoucher> getTouristVoucher() {
        return touristVoucher;
    }

    void readData() {
        NodeList nodeList = document.getElementsByTagName("touristVoucher");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                int id = Integer.parseInt(element.getAttribute("id"));
                String type = getTextContent(element, "type", 0);
                String country = getTextContent(element, "country", 0);
                int quantityDays = getIntegerContent(element, "quantityDays", 0);
                int quantityNights = getIntegerContent(element, "quantityNight", 0);
                TransportType transport = MyParser.getTransportType(getTextContent(element, "transport", 0));
                Hotel hotel = getHotel(getElement(element.getElementsByTagName("hotel")));
                touristVoucher.add(
                        new TouristVoucher(id, type, country, quantityDays, quantityNights, transport, hotel));
            }
        }
    }

    private Hotel getHotel(Element element) {
        String name = getTextContent(element, "name", 0);
        double rating = getDoubleContent(element, "rating", 0);
        Apartment apartment = getApartment(element);
        HotelService hotelService = getHotelService(element);
        return new Hotel(name, rating, apartment, hotelService);
    }

    private Apartment getApartment(Element element) {
        int id = getIntegerContent(element, "id", 0);
        int quantityPerson = getIntegerContent(element, "quantityPerson", 0);
        return new Apartment(id, quantityPerson);
    }

    private HotelService getHotelService(Element element) {
        boolean meal = getBooleanContent(element, "meal", 0);
        boolean tv = getBooleanContent(element, "TV", 0);
        boolean condition = getBooleanContent(element, "conditioner", 0);
        return new HotelService(meal, tv, condition);
    }

    private int getIntegerContent(Element element, String tag, int item) {
        return Integer.parseInt(getTextContent(element, tag, item));
    }

    private double getDoubleContent(Element element, String tag, int item) {
        return Double.parseDouble(getTextContent(element, tag, item));
    }

    private boolean getBooleanContent(Element element, String tag, int item) {
        return Boolean.parseBoolean(getTextContent(element, tag, item));
    }

    private String getTextContent(Element element, String tag, int item) {
        return element.getElementsByTagName(tag).item(item).getTextContent();
    }

    private Element getElement(NodeList node) {
        return (Element) node.item(0);
    }
}
