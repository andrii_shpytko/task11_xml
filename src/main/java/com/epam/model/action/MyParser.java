package com.epam.model.action;

import com.epam.model.action.parsers.dom.DOMParser;
import com.epam.model.action.parsers.sax.MySAXParser;
import com.epam.model.action.parsers.stax.STAXParser;
import com.epam.model.entity.TouristVoucher;
import com.epam.model.entity.TransportType;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public abstract class MyParser {
    private File xmlFile;
    private File xsdFile;

    public MyParser(String xmlFileName, String xsdFileName) throws FileNotFoundException {
        setXmlFile(xmlFileName);
        setXsdFile(xsdFileName);
    }

    public File getXmlFile() {
        return xmlFile;
    }

    public void setXmlFile(String xmlFileName) throws FileNotFoundException {
        this.xmlFile = new FileLoader().getFile("xml" + "/" + xmlFileName);
    }

    public File getXsdFile() {
        return xsdFile;
    }

    public void setXsdFile(String xsdFileName) throws FileNotFoundException {
        this.xsdFile = new FileLoader().getFile("xml" + "/" + xsdFileName);
    }

    public abstract List<TouristVoucher> parseFromFile() throws IOException, SAXException, XMLStreamException;

    public static List<TouristVoucher> parseFromFile(String parserType, String fileName)
            throws ParserConfigurationException, SAXException, IOException, XMLStreamException {
        String xmlFileName = fileName + ".xml";
        String xsdFileName = fileName + ".xsd";
        switch (parserType.toUpperCase()) {
            case "DOM":
                return new DOMParser(xmlFileName, xsdFileName).parseFromFile();
            case "SAX":
                return new MySAXParser(xmlFileName, xsdFileName).parseFromFile();
            case "STAX":
                return new STAXParser(xmlFileName, xsdFileName).parseFromFile();
            default:
                throw new IllegalArgumentException("invalid parse type!");
        }
    }

    public static TransportType getTransportType(String transportType) {
        switch (transportType) {
            case "car":
                return TransportType.CAR;
            case "bus":
                return TransportType.BUS;
            case "train":
                return TransportType.TRAIN;
            case "airline":
                return TransportType.AIRPLANE;
            case "ship":
                return TransportType.SHIP;
            default:
                throw new IllegalArgumentException("unsupported transport instance [" + transportType + "]");
        }
    }
}