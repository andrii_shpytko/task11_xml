package com.epam.model.entity;

public class TouristVoucher {
    private int id;
    private String tripType;
    private String country;
    private int quantityDays;
    private int quantityNights;
    private TransportType transportType;
    private Hotel hotel;

    public TouristVoucher() {
    }

    public TouristVoucher(int id, String tripType, String country, int quantityDays, int quantityNights,
                          TransportType transportType, Hotel hotel) {
        this.id = id;
        this.tripType = tripType;
        this.country = country;
        this.quantityDays = quantityDays;
        this.quantityNights = quantityNights;
        this.transportType = transportType;
        this.hotel = hotel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getQuantityDays() {
        return quantityDays;
    }

    public void setQuantityDays(int quantityDays) {
        this.quantityDays = quantityDays;
    }

    public int getQuantityNights() {
        return quantityNights;
    }

    public void setQuantityNights(int quantityNights) {
        this.quantityNights = quantityNights;
    }

    public TransportType getTransportType() {
        return transportType;
    }

    public void setTransportType(TransportType transportType) {
        this.transportType = transportType;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    @Override
    public String toString() {
        return "TouristVoucher{" +
                "id=" + id +
                ", tripType='" + tripType + '\'' +
                ", country='" + country + '\'' +
                ", quantityDays=" + quantityDays +
                ", quantityNights=" + quantityNights +
                ", transportType=" + transportType +
                ", hotel=" + hotel +
                '}';
    }
}
