package com.epam.model.entity;

public enum TransportType {
    CAR,
    BUS,
    TRAIN,
    AIRPLANE,
    SHIP
}
