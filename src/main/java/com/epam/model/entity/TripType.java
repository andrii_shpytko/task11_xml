package com.epam.model.entity;

public enum TripType {
    WEEKEND,
    EXCURSION,
    PILGRIMAGE
}
