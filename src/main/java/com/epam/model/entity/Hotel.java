package com.epam.model.entity;

import com.epam.model.entity.hotelService.HotelService;

public class Hotel {
    private String hotelName;
    private double hotelRating;
    private Apartment apartment;
    private HotelService hotelService;

    public Hotel() {
    }

    public Hotel(String hotelName, double hotelRating, Apartment apartment, HotelService hotelService) {
        this.hotelName = hotelName;
        this.hotelRating = hotelRating;
        this.apartment = apartment;
        this.hotelService = hotelService;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public double getHotelRating() {
        return hotelRating;
    }

    public void setHotelRating(double hotelRating) {
        this.hotelRating = hotelRating;
    }

    public Apartment getApartment() {
        return apartment;
    }

    public void setApartment(Apartment apartment) {
        this.apartment = apartment;
    }

    public HotelService getHotelService() {
        return hotelService;
    }

    public void setHotelService(HotelService hotelService) {
        this.hotelService = hotelService;
    }

    @Override
    public String toString() {
        return "Hotel{" +
                "hotelName='" + hotelName + '\'' +
                ", hotelRating=" + hotelRating +
                ", apartment=" + apartment +
                ", hotelService=" + hotelService +
                '}';
    }
}
