package com.epam.model.entity.hotelService;

public class HotelService {
    private boolean meal;
    private boolean tv;
    private boolean conditioner;
    private boolean wiFi;

    public HotelService() {
    }

    public HotelService(boolean meal, boolean tv, boolean conditioner) {
        this.meal = meal;
        this.tv = tv;
        this.conditioner = conditioner;
    }

    public boolean isMeal() {
        return meal;
    }

    public void setMeal(boolean meal) {
        this.meal = meal;
    }

    public boolean isTv() {
        return tv;
    }

    public void setTv(boolean tv) {
        this.tv = tv;
    }

    public boolean isConditioner() {
        return conditioner;
    }

    public void setConditioner(boolean conditioner) {
        this.conditioner = conditioner;
    }

    public boolean isWiFi() {
        return wiFi;
    }

    public void setWiFi(boolean wiFi) {
        this.wiFi = wiFi;
    }
}
