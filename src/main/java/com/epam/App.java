package com.epam;

import com.epam.view.MainMenu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class App {
    private static final Logger LOGGER = LogManager.getLogger(App.class);
    public static void main(String[] args) {
        System.out.println("Hello World!");
        LOGGER.info("Hello World!");
        new MainMenu().launch();
    }
}
