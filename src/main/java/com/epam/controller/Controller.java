package com.epam.controller;

import com.epam.model.action.MyParser;
import com.epam.model.entity.TouristVoucher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.List;

public class Controller {
    private static final Logger LOGGER = LogManager.getLogger(Controller.class);

    public void getTouristVouchersDOM(String fileName) {
        try {
            List<TouristVoucher> touristVouchers = MyParser.parseFromFile("DOM", fileName);
            showTouristVouchers(touristVouchers);
        } catch (ParserConfigurationException | SAXException | IOException | XMLStreamException e) {
            e.printStackTrace();
        }
    }

    public void getTouristVouchersSAX(String fileName) {
        try {
            List<TouristVoucher> touristVouchers = MyParser.parseFromFile("SAX", fileName);
            showTouristVouchers(touristVouchers);
        } catch (ParserConfigurationException | SAXException | IOException | XMLStreamException e) {
            e.printStackTrace();
        }
    }

    public void getTouristVouchersSTAX(String fileName) {
        try {
            List<TouristVoucher> touristVouchers = MyParser.parseFromFile("STAX", fileName);
            showTouristVouchers(touristVouchers);
        } catch (ParserConfigurationException | SAXException | IOException | XMLStreamException e) {
            e.printStackTrace();
        }
    }

    private void showTouristVouchers(List<TouristVoucher> touristVouchers) {
        touristVouchers.sort((TouristVoucher t1, TouristVoucher t2) -> t1.getId() - t2.getId());
        touristVouchers.forEach((TouristVoucher touristVoucher) ->
                {
                    LOGGER.info("touristVoucherId: " + touristVoucher.getId());
                    LOGGER.info("Type: " + touristVoucher.getTripType());
                    LOGGER.info("Country: " + touristVoucher.getCountry());
                    LOGGER.info("quantityDays: " + touristVoucher.getQuantityDays());
                    LOGGER.info("quantityNights: " + touristVoucher.getQuantityNights());
                    LOGGER.info("TransportType: " + touristVoucher.getTransportType());
                    LOGGER.info("Hotel:");
                    LOGGER.info("\tname: " + touristVoucher.getHotel().getHotelName());
                    LOGGER.info("\trating: " + touristVoucher.getHotel().getHotelRating());
                    LOGGER.info("\tApartment: ");
                    LOGGER.info("\t\tidApartment: " + touristVoucher.getHotel().getApartment().getIdNumber());
                    LOGGER.info("\t\tquantityPersons: " + touristVoucher.getHotel().getApartment().getQuantityPerson());
                    LOGGER.info("\tServices:");
                    LOGGER.info("\t\tmeal: " + touristVoucher.getHotel().getHotelService().isMeal());
                    LOGGER.info("\t\tTV: " + touristVoucher.getHotel().getHotelService().isTv());
                    LOGGER.info("\t\tconditioner: " + touristVoucher.getHotel().getHotelService().isConditioner());
                }
        );
    }

}
