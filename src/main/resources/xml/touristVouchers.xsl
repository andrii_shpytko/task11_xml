<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <style type="text/css">
                    th {
                    background-color: rgb(240, 240, 240);
                    height: 50px;
                    color: black;
                    }
                    td {
                    height: 50px;
                    text-align: center;
                    border-right: 1px solid black;
                    }
                </style>
            </head>
            <body>
                <table style="knifeTable">
                    <tr>
                        <th style="width:200px">ID</th>
                        <th style="width:200px">Type</th>
                        <th style="width:250px">Country</th>
                        <th style="width:250px">quantityDays</th>
                        <th style="width:250px">quantityNights</th>
                        <th style="width:250px">transportType</th>
                        <th style="width:250px">hotelName</th>
                        <th style="width:250px">hotelRating</th>
                        <th style="width:150px">idApartment</th>
                        <th style="width:150px">quantityPersons</th>
                        <th style="width:100px">Meal</th>
                        <th style="width:100px">TV</th>
                        <th style="width:100px">Conditioner</th>
                    </tr>
                    <xsl:for-each select="touristVouchers/touristVoucher">
                        <tr>
                            <td>
                                <xsl:value-of select="@id" />
                            </td>
                            <td>
                                <xsl:value-of select="tripType" />
                            </td>
                            <td>
                                <xsl:value-of select="country" />
                            </td>
                            <td>
                                <xsl:value-of select="quantityDays" />
                            </td>
                            <td>
                                <xsl:value-of select="quantityNights" />
                            </td>
                            <td>
                                <xsl:value-of select="transportType" />
                            </td>
                            <td>
                                <xsl:value-of select="hotel/hotelName" />
                            </td>
                            <td>
                                <xsl:value-of select="hotel/hotelRating" />
                            </td>
                            <td>
                                <xsl:value-of select="hotel/apartment/idNumber" />
                            </td>
                            <td>
                                <xsl:value-of select="hotel/apartment/quantityPerson" />
                            </td>
                            <td>
                                <xsl:value-of select="hotel/services/meal" />
                            </td>
                            <td>
                                <xsl:value-of select="hotel/services/TV" />
                            </td>
                            <td>
                                <xsl:value-of select="hotel/services/conditioner" />
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>

    </xsl:template>
</xsl:stylesheet>